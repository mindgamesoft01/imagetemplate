package com.mindgame.image.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mindgame.image.CustomDialogClass;
import com.mindgame.image.Imageview_SetOne;
import com.mindgame.image.NetworkStatusCheck;
import com.mindgame.image.R;


import java.util.ArrayList;

/**
 * Created by lenovo on 16-Apr-17.
 */
public class  DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<String> loadingimags, thumbs;
    private Context context;
    final NetworkStatusCheck NC = NetworkStatusCheck.getInstance();
    CustomDialogClass cd;



    public DataAdapter(Context context, ArrayList<String> loading_imags, ArrayList<String> thumbs) {
        this.context = context;
        this.loadingimags = loading_imags;
        this.thumbs = thumbs;

    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        String str = (String) this.thumbs.get(i);
        setUpImage(viewHolder.img, str);

       /* Glide.with(context).load(loadingimags.get(i))
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.img);*/
        // viewHolder.img.setImageURI(loadingimags.get(i));
        //     Picasso.with(context).load(loadingimags.get(i).getImage_url()).resize(120, 60).into(viewHolder.img);
        //  Picasso.with(context).load(loadingimags.get(i).getImage_url()).into(viewHolder.img);

        //Picasso.with(context).load(loadingimags.get(i).getImage_url()).resize(120, 60).into(viewHolder.img);
        //Glide.with(context).load(str).into(viewHolder.img);

    }


    @Override
    public int getItemCount() {
        return loadingimags.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img;

        public ViewHolder(final View view) {
            super(view);

            img = (ImageView) view.findViewById(R.id.img1);



            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (NC.isOnline(context) == Boolean.TRUE && NC.json_status == Boolean.TRUE) {

                        Intent localIntent = new Intent(context, Imageview_SetOne.class);
                        Bundle galleryBundle = new Bundle();
                        galleryBundle.putStringArrayList("images", loadingimags);
                        galleryBundle.putInt("POS", getAdapterPosition());
                        localIntent.putExtras(galleryBundle);
                        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(localIntent);
                    }
                    else {
                        Toast.makeText(context, "Please connect to the internet", Toast.LENGTH_SHORT).show();

                    }
                    //context.startActivity(new Intent(v.getContext(), ImageviewActivity.class).putExtra("POS", getPosition()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        }

        @Override
        public void onClick(View v) {

        }
    }

    private void setUpImage(ImageView paramImageView, String paramString) {
        if (!TextUtils.isEmpty(paramString)) {


            Glide.with(paramImageView.getContext()).load(paramString)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(paramImageView);
            //Picasso.with(paramImageView.getContext()).load(paramString).resize(this.mGridItemWidth,this.mGridItemHeight).centerCrop().into(paramImageView);
            return;
        }
        paramImageView.setImageDrawable(null);
    }
}