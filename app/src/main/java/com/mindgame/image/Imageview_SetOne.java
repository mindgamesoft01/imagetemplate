package com.mindgame.image;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.Thing;
import com.mindgame.image.adapter.CustomPagerAdapter;
import com.mindgame.image.indicator.CirclePageIndicator;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class Imageview_SetOne extends Activity {

    CustomPagerAdapter customPagerAdapter;
    CirclePageIndicator indicator;
    ViewPager viewPager;
    TextView indicateimages;
    int val, flag;
    private List<String> mImages;
    private ArrayList<String> localarraaylist_fullscreen;
    int position;
    LinearLayout layout, strip, layout1, strip1;
    //AdClass ad = new AdClass();
    AdMobdetails_singleton ad = AdMobdetails_singleton.getInstance();
    //String Banner_Id = AdMobdetails_singleton.getInstance().get_admob_id(0,"banner");
    final NetworkStatusCheck NC = NetworkStatusCheck.getInstance();
    CustomDialogClass cd;
    boolean isfirest = true;
    boolean adddate = true;
    boolean adddatefirst = true;


    SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageview);
        localarraaylist_fullscreen = new ArrayList<>();
        cd = new CustomDialogClass(Imageview_SetOne.this, 1);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        isfirest = pref.getBoolean("RATE", true);


        adddate = pref.getBoolean("Date", true);
        if (adddate == true) {

            Calendar calendar = Calendar.getInstance();

            int thisYear = calendar.get(Calendar.YEAR);

            int thisMonth = calendar.get(Calendar.MONTH);

            int thisDay = calendar.get(Calendar.DAY_OF_MONTH);


            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("Date", false);
            editor.putInt("date", thisDay);
            editor.putInt("month", thisMonth);
            editor.putInt("year", thisYear);

            editor.commit();
        }

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner(getApplicationContext());


        indicateimages = (TextView) findViewById(R.id.indicateimages);
        //final ArrayList image_Urls = prepareData();
        Bundle fb = getIntent().getExtras();
        this.mImages = fb.getStringArrayList("images");
        this.position = fb.getInt("POS");
        localarraaylist_fullscreen.addAll(this.mImages);


        customPagerAdapter = new CustomPagerAdapter(this, localarraaylist_fullscreen);
        viewPager = (ViewPager) findViewById(R.id.viewpager_suit);
        viewPager.setClipToPadding(false);
        viewPager.setPageMargin(25);

        viewPager.setPadding(45, 0, 45, 0);

        viewPager.setAdapter(customPagerAdapter);


        viewPager.setCurrentItem(position);
        indicateimages.setText(viewPager.getCurrentItem() + 1 + "/" + localarraaylist_fullscreen.size());

        findViewById(R.id.saveImagebutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    svaeImage();
                    val = 0;
                } else if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                    cd.show();
                }


            }

        });
        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    svaeImage();
                    val = 1;
                } else if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                    cd.show();
                }


            }

        });

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            // optional
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            // optional
            @Override
            public void onPageSelected(int position) {
            }

            // optional
            @Override
            public void onPageScrollStateChanged(int state) {

                if (viewPager.getCurrentItem() == 4) {

                    isfirest = pref.getBoolean("RATE", true);

                    if (isfirest == true) {
                        adddatefirst = pref.getBoolean("Dates", true);
                        if (adddatefirst == true) {
                            cd.customDailog();
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putBoolean("Dates", false);
                            editor.commit();


                        } else {
                            int year = pref.getInt("year", 2017);
                            int month = pref.getInt("month", 2017);

                            int date = pref.getInt("date", 2017);


                            Calendar start = Calendar.getInstance();
                            Calendar end = Calendar.getInstance();
                            start.set(year, month, date);

                            Calendar calendar = Calendar.getInstance();

                            int thisYear = calendar.get(Calendar.YEAR);

                            int thisMonth = calendar.get(Calendar.MONTH);

                            int thisDay = calendar.get(Calendar.DAY_OF_MONTH);

                            end.set(thisYear, thisMonth, thisDay);

                            Date startDate = start.getTime();
                            Date endDate = end.getTime();
                            long startTime = startDate.getTime();
                            long endTime = endDate.getTime();
                            long diffTime = endTime - startTime;
                            long diffDays = diffTime / (1000 * 60 * 60 * 24);

                            if (diffDays > 15) {

                                cd.customDailog();
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putBoolean("Date", false);
                                editor.putInt("date", thisDay);
                                editor.putInt("month", thisMonth);
                                editor.putInt("year", thisYear);

                                editor.commit();


                            }

                        }
                    } else {
                        int year = pref.getInt("year", 2017);
                        int month = pref.getInt("month", 2017);

                        int date = pref.getInt("date", 2017);


                        Calendar start = Calendar.getInstance();
                        Calendar end = Calendar.getInstance();
                        start.set(year, month, date);

                        Calendar calendar = Calendar.getInstance();

                        int thisYear = calendar.get(Calendar.YEAR);

                        int thisMonth = calendar.get(Calendar.MONTH);

                        int thisDay = calendar.get(Calendar.DAY_OF_MONTH);

                        end.set(thisYear, thisMonth, thisDay);

                        Date startDate = start.getTime();
                        Date endDate = end.getTime();
                        long startTime = startDate.getTime();
                        long endTime = endDate.getTime();
                        long diffTime = endTime - startTime;
                        long diffDays = diffTime / (1000 * 60 * 60 * 24);

                        if (diffDays > 30) {

                            cd.customDailog();
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putBoolean("Date", false);
                            editor.putInt("date", thisDay);
                            editor.putInt("month", thisMonth);
                            editor.putInt("year", thisYear);

                            editor.commit();


                        }

                    }

                }

                indicateimages.setText(viewPager.getCurrentItem() + 1 + "/" + localarraaylist_fullscreen.size());

//                else if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                    //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
//                    cd .show();
//                }


            }
        });

        findViewById(R.id.myimags).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), MyImages.class));


            }
        });

        findViewById(R.id.rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));


            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    private void svaeImage() {


        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
            new AsyncTaskLoadImage().execute(localarraaylist_fullscreen.get(viewPager.getCurrentItem()));

        } else {
            Toast.makeText(getApplicationContext(), "Please Check NetWork Connection", Toast.LENGTH_LONG).show();
        }
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Imageview Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
    }


    public class AsyncTaskLoadImage extends AsyncTask<String, String, Bitmap> {
        private final static String TAG = "AsyncTaskLoadImage";
        ProgressDialog ringProgressDialog;

        public AsyncTaskLoadImage() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ringProgressDialog = ProgressDialog.show(Imageview_SetOne.this, "Please wait ...", "Downloading Image ...", true);

            ringProgressDialog.setCancelable(true);

        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                URL url = new URL(params[0]);
                bitmap = BitmapFactory.decodeStream((InputStream) url.getContent());


            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {


            ringProgressDialog.dismiss();


            shareandSave(bitmap);

        }
    }

    private void shareandSave(Bitmap bmp) {
        if (val == 1) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/*");

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.jpg");
            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temp.jpg"));
            try {
                startActivity(Intent.createChooser(share, "Share via"));
            } catch (ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(), "Please Connect To Internet", Toast.LENGTH_LONG)
                        .show();
            }

        } else {
            saveImageToExternalStorage(bmp);
        }
    }

    void saveImageToExternalStorage(Bitmap b) {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        String timeStamp = sf.format(new Date());

        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/sareedesigns/";

        String temp_path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;


        try {
            File dir_temp = new File(temp_path);
            if (!dir_temp.exists()) {
                dir_temp.mkdirs();
            }
            OutputStream fOut_temp = null;
            File file_temp = new File(temp_path, "sareedesigns" + ".jpg");
            if (file_temp.exists())
                file_temp.delete();
            file_temp.createNewFile();
            fOut_temp = new FileOutputStream(file_temp);
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.JPEG, 100, fOut_temp);
            fOut_temp.flush();
            fOut_temp.close();
        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
        }

        try {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            OutputStream fOut = null;
            File file = new File(fullPath, "picture" + timeStamp + ".jpg");
            if (file.exists())
                file.delete();
            file.createNewFile();
            fOut = new FileOutputStream(file);
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
        }

    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (ad.adView != null) {
            ad.adView.pause();
        }
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        //app_name.setText(singleton_images.getInstance().getApp_name());
        if (ad.adView != null) {
            ad.adView.resume();
        }
    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {

        if (ad.adView != null) {
            ad.adView.destroy();
        }
        super.onDestroy();
    }

   /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        cd.customDailog();
        return super.onKeyDown(keyCode, event);
    }*/


    @Override
    public void onBackPressed() {


        super.onBackPressed();


    }
}

