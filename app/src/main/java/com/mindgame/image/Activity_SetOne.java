package com.mindgame.image;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.mindgame.image.adapter.DataAdapter;


import java.util.ArrayList;

public class Activity_SetOne extends Activity {
    // Remote Config keys
//    private static final String LOADING_PHRASE_CONFIG_KEY = "loading_phrase";
    private static final String WELCOME_MESSAGE_KEY = "welcome_message";
//    private static final String WELCOME_MESSAGE_CAPS_KEY = "welcome_message_caps";

    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    //TextView decleration
    TextView textView;
    InterstitialAd interstitialAd;

    LinearLayout layout, strip, layout1, strip1;
    AdMobdetails_singleton ad = AdMobdetails_singleton.getInstance() ;
    TextView Title1;
    ProgressDialog pd;
    int index;
    final NetworkStatusCheck NC = NetworkStatusCheck.getInstance();
    CustomDialogClass cd;



    public ArrayList<String> images_json, images_thumbs_json;
    //URLs of the images to load in the gallery1



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //assigning the layout
        setContentView(R.layout.activity_main);
        /*Get the JSON data from firebase database*/
        Bundle b=getIntent().getExtras();

        index= b.getInt("flag");
//       Toast.makeText(getApplicationContext(),index,Toast.LENGTH_SHORT).show();



        //AdMob();
        images_json = new ArrayList<String>();
        images_thumbs_json = new ArrayList<String>();
        Title1 = (TextView) findViewById(R.id.heading);
        Title1.setText(singleton_images.getInstance().getApp_name());
        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner(getApplicationContext());


        images_json = singleton_images.getInstance().get_set_images(index);
        images_thumbs_json = singleton_images.getInstance().get_set_images_thumbs(index);

        initViews();

    }


    private void initViews() {
        //registering the textview
        textView=(TextView)findViewById(R.id.heading);
    //recycler view registration
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        //when you are adding or removing items in the RecyclerView and that doesn't change it's height or the width.
        recyclerView.setHasFixedSize(true);
        //assigning gridlayout to the recyclerview in two columns
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(layoutManager);

      //Assigning the ulrs to the arraylist
        //ArrayList image_Urls = prepareData();
        //sending the array list to the dataadapter class

        DataAdapter adapter = new DataAdapter(getApplicationContext(), images_json, images_thumbs_json);
        //set the adapter to the recycler view
        recyclerView.setAdapter(adapter);


    }


    /** Called when leaving the activity */
    @Override
    public void onPause() {
        if ( ad.adView!= null) {
            ad.adView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        //app_name.setText(singleton_images.getInstance().getApp_name());
        if (ad.adView!= null) {
            ad.adView.resume();
        }
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {

        if (ad.adView!= null) {
            ad.adView.destroy();
        }
        super.onDestroy();
    }

}
