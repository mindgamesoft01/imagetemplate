package com.mindgame.image;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class Activity_00 extends Activity {


	LinearLayout layout, strip, layout1, strip1;
	AdMobdetails_singleton ad = AdMobdetails_singleton.getInstance() ;
	ProgressBar spinner;
	Boolean network,network_check;
	Context c;
	PopupWindow pop;
	InterstitialAd interstitialAd;
	String interstitial;
	//AdMobdetails_singleton AdMob=AdMobdetails_singleton.getInstance();
	String INTERSTITIAL_KEY="interstitial";
	TextView app_name, terms_privacy;;
    ProgressDialog pd;
	String admob_mode, image_mode;
	AppStatus appStatus;
	TextView networkcheck;
	Button gallery,favourites,rate,refresh;
	RelativeLayout relativeLayout;
	LinearLayout linearLayout;
	Boolean JSON_STATUS ;
	CustomDialogClass cd;
	final NetworkStatusCheck NC = NetworkStatusCheck.getInstance();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_00);
		cd=new CustomDialogClass(Activity_00.this,0);

		init();


		c = getApplicationContext();
		appStatus = new AppStatus();
		network_check = appStatus.isOnline(getApplicationContext());
		if (NC.isOnline(getApplicationContext()) == Boolean.FALSE && NC.json_status == Boolean.FALSE) {
			//Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_LONG).show();
			cd.show();
			networkcheck.setVisibility(View.VISIBLE);
			relativeLayout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
				}
			});
			linearLayout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();

				}
			});
			gallery.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Toast.makeText(getApplicationContext(), "Please connect to the internet click refresh button", Toast.LENGTH_LONG).show();

				}
			});
			favourites.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();

				}
			});
			rate.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();

				}
			});
		}
		if (network_check == Boolean.TRUE) {
			networkcheck.setVisibility(View.INVISIBLE);
			pd = new ProgressDialog(this);
			pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pd.setMessage("Please wait ..." + "Loading ...");
			pd.setIndeterminate(true);
			pd.setCancelable(false);
			pd.show();
			String APP_JSON_NAME = getPackageName().replace(".", "_") + ".json";
			NC.json_status = getJSONdata(APP_JSON_NAME);
			//Toast.makeText(getApplicationContext(), "" + JSON_STATUS, Toast.LENGTH_LONG).show();
			if (Boolean.TRUE) {



				layout = (LinearLayout) findViewById(R.id.admob);
				terms_privacy = (TextView) findViewById(R.id.privacy);
				terms_privacy.setPaintFlags(terms_privacy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
					/*----------------------------------PRIVACY BUTTON------------------------------*/
				terms_privacy.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (NC.isOnline(getApplicationContext()) == Boolean.TRUE && NC.json_status == Boolean.TRUE) {
							Intent i = new Intent(Activity_00.this, PrivacyPolicy.class);
							startActivity(i);
						}
						else  {
							//Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
							cd.show();
						}
					}
				});
				/*--------------------------GALLERY BUTTON-----------------------------------------*/
				gallery.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						if (NC.isOnline(getApplicationContext()) == Boolean.TRUE && NC.json_status == Boolean.TRUE) {

							Intent i = new Intent(Activity_00.this, Activity_01.class);
							i.putExtra("ads", true);
							startActivity(i);

						} else {
							//Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
							cd.show();
						}
					}


				});
				/*--------------------------FAVORITES BUTTON-----------------------------------------*/
				favourites.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {


						if (NC.isOnline(getApplicationContext()) == Boolean.TRUE && NC.json_status == Boolean.TRUE) {
							Intent i = new Intent(Activity_00.this, MyImages.class);

							startActivity(i);
						}
						else {
							//Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
							cd.show();
						}

					}


				});
				/*--------------------------RATE BUTTON-----------------------------------------*/
				rate.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (NC.isOnline(getApplicationContext()) == Boolean.TRUE && NC.json_status == Boolean.TRUE) {

							Intent i = new Intent(
									Intent.ACTION_VIEW,
									Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName()));

							startActivity(i);
						}
						else {
							//Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
							cd.show();
						}


					}


				});

			}

		}
		refresh.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Activity_00.this, Activity_00.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);

			}
		});
	}



	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}

		return super.onKeyDown(keyCode, event);
	}

    /*-----------READING FROM FIREBASE DB---------------------------------------------------------------------
     *
     * @param JSON_name
     *---------------------------------------------------------------------------------------------------------*/
    public void getJSONFromFirebase_Old(String JSON_name) {

        final singleton_images singleton = singleton_images.getInstance();
        final String TAG = "FIREBASE";
        FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference =    mFirebaseDatabase.getReference(JSON_name);


    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {


//                JSONArray tst = dataSnapshot.get
            JSONArray test_images = new JSONArray();
            JSONArray prod_images = new JSONArray();


            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                String KEY = childDataSnapshot.getKey();
                Log.v(TAG,""+ childDataSnapshot.getKey()); //displays the key for the node
                Log.v(TAG,""+ childDataSnapshot.getValue());   //gives the value for given keyname
                try {

                    switch (KEY) {
                        case "ad_token":
                            JSONArray ad_details = new JSONArray(childDataSnapshot.getValue().toString());
                            singleton.setAd_details(ad_details);
                            break;
                        case "prod_images":
                            prod_images = new JSONArray(childDataSnapshot.getValue().toString());
                            break;
                        case "test_images":
                            test_images = new JSONArray(childDataSnapshot.getValue().toString());
                            break;

                        case "app_name":
                            app_name.setText(childDataSnapshot.getValue(String.class));
                            singleton.setApp_name(childDataSnapshot.getValue(String.class));
                            break;

                        case "admob_details":
                            JSONObject admob_details;
                            HashMap<String,Object> mapobj = (HashMap<String, Object>) childDataSnapshot.getValue();
                            admob_details = new JSONObject(mapobj);
                            singleton.setAdmob_details(admob_details);
                            break;

                        case "interstitial_timer":
                            singleton.setInterstitial_timer( Integer.valueOf(childDataSnapshot.getValue().toString()));
                            break;

                        case "app_mode":
                            singleton.app_mode = childDataSnapshot.getValue(String.class);
                            break;

                    }



                } catch (Exception e) {
                    e.printStackTrace();
                }
            } /*for loop ends here*/

            if (singleton.app_mode.equals("test")) {
                singleton.images_json = test_images;

            } else {
                singleton.images_json = prod_images;
            }
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });

/*-----------READING FROM FIREBASE DB----------------------------------------------------------------------*/


}

    /**-------------------------APP_JSON-----------------------------------------------*
     * Get the JSON name from the app package as Key. in JSON, "." is not allowed.
     * so need to replace "." with _
     * Once we get the package name, then use Volley API to get the JSON data.
     ---------------------------------------------------------------------------------*/

    String APP_JSON_NAME = "";
public void get_app_json() {
    final singleton_images singleton = singleton_images.getInstance();
    final String TAG = "FIREBASE";

    FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
    String pacakage_name = "app_database/" + getPackageName().replace(".", "_");
    DatabaseReference databaseReference =    mFirebaseDatabase.getReference(pacakage_name);

    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            APP_JSON_NAME = dataSnapshot.getValue(String.class);
            getJSONdata(APP_JSON_NAME);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });
}

    public Boolean getJSONdata(String APP_JSON_NAME){


		RequestQueue mRequestQueue;

// Instantiate the cache
		Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap

// Set up the network to use HttpURLConnection as the HTTP client.
		Network network = new BasicNetwork(new HurlStack());

// Instantiate the RequestQueue with the cache and network.
		mRequestQueue = new RequestQueue(cache, network);

// Start the queue
		mRequestQueue.start();


//		String url = "https://robot01-a668c.firebaseio.com/" + APP_JSON_NAME.trim();
		String url = "http://139.59.55.102:5555/production/" + "com_mindgame_girl_hairstyles_app01";
		Log.d("APP JSON URL:", url);


		JsonObjectRequest jsObjRequest = new JsonObjectRequest
				(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
//						Log.d("JSON", response.toString());
						JSONArray app_images = new JSONArray();
						try {

							singleton_images singleton = singleton_images.getInstance();
							admob_mode = response.getString("admob_mode");
							image_mode = response.getString("image_mode");

							/*Set the modes in Singleton*/
							singleton.admob_mode = admob_mode;
							singleton.image_mode = image_mode;
							AdMobdetails_singleton.getInstance().admob_mode = admob_mode;

							/*Handling IMAGE_MODE Flag*/
							switch (image_mode) {

								case "test":
									app_images = response.getJSONArray("test_images");
									break;
								case "prod":
									app_images = response.getJSONArray("prod_images");
									break;
								case "hold":
									app_images = new JSONArray();
									break;
							}

							/*Handling ADMOB_MODE Flag*/
							switch (admob_mode) {

								case "test":

									break;
								case "prod":

									break;
								case "hold":

									break;
							}


							JSONArray ad_details = response.getJSONArray("ad_token");
							JSONObject admob_details = response.getJSONObject("admob_details");
							/*Set the Ad details in Admob*/

							AdMobdetails_singleton.getInstance().setAdmob_details(admob_details);
							app_name.setText(response.getString("app_name"));
							singleton.setInterstitial_timer( response.getInt("interstitial_timer"));
							singleton.setApp_name(response.getString("app_name"));
							singleton.setAdmob_details(admob_details);


							singleton.setImages_json(app_images);
							singleton.setAd_details(ad_details);
                            //dismissing the dialog spinner
							pd.dismiss();


							strip = ad.layout_strip(getApplicationContext());
							layout.addView(strip);
							ad.AdMobBanner(getApplicationContext());



							pop=new PopupWindow(getApplicationContext());
							interstitial=ad.get_admob_id(4,INTERSTITIAL_KEY);



						}
						catch (Exception e){
							e.printStackTrace();

						}




					}
				},


						new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e("JSON", error.toString());


						// TODO Auto-generated method stub
						//
						}

				});


// Add the request to the RequestQueue.
		NC.json_status=Boolean.TRUE;
		Log.d("JSON_STATUS :" ,NC.json_status.toString());
		mRequestQueue.add(jsObjRequest);
		return NC.json_status;

	}

	/** Called when leaving the activity */
	@Override
	public void onPause() {
		if ( ad.adView!= null) {
			ad.adView.pause();
		}
		super.onPause();
	}

	/** Called when returning to the activity */
	@Override
	public void onResume() {
		super.onResume();
		app_name.setText(singleton_images.getInstance().getApp_name());
		if (ad.adView!= null) {
			ad.adView.resume();
		}
	}

	/** Called before the activity is destroyed */
	@Override
	public void onDestroy() {

		if (ad.adView!= null) {
			ad.adView.destroy();
		}
		super.onDestroy();
	}
	public void init(){
		app_name = (TextView) findViewById(R.id.app_name);
		networkcheck=(TextView)findViewById(R.id.network_checking);
		favourites=(Button)findViewById(R.id.favourite);
		gallery=(Button)findViewById(R.id.gallery);
		rate=(Button)findViewById(R.id.rate);
		relativeLayout=(RelativeLayout)findViewById(R.id.relative_total);
		linearLayout=(LinearLayout)findViewById(R.id.linear_total);
		refresh=(Button)findViewById(R.id.refresh);

	}


}
