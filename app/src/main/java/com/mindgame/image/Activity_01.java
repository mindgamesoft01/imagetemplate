package com.mindgame.image;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Handler;

public class Activity_01 extends AppCompatActivity {

    ImageView img1,img2,img3,img4,img5,img6,img7,img8,img9,img10;
    TextView txt1,txt2,txt3,txt4,txt5,txt6,txt7,txt8,txt9,txt10;
    LinearLayout layout_1,layout_2,layout_3,layout_4,layout_5,layout_6,layout_7,layout_8,layout_9,layout_10;
    ArrayList<String>images_json;
    Context context;
    InterstitialAd interstitialAd;
    AdMobdetails_singleton AdMob = AdMobdetails_singleton.getInstance();
    String interstitial0, interstitial1,
             interstitial2,interstitial3,interstitial4;
    final String INTERSTITIAL_KEY = "interstitial";
    AdRequest adRequest = new AdRequest.Builder().build();
    Handler handler;

    LinearLayout layout, strip, layout1, strip1;
//    ProgressDialog ringProgressDialog=new ProgressDialog(this);
    ProgressDialog pd;
    Context c;
    Boolean ads_variable,value;
    String TAG=Activity_01.this.getClass().getSimpleName();

    java.sql.Timestamp timestamp;
    long current_time;
    TextView Title2;
    Boolean network_check,network_state;
    private BroadcastReceiver mNetworkReceiver;
    final NetworkStatusCheck NC = NetworkStatusCheck.getInstance();

    CustomDialogClass CD;
    AdMobdetails_singleton ad = AdMobdetails_singleton.getInstance() ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        mNetworkReceiver = new NetworkChangeReceiver();
        CD=new CustomDialogClass(Activity_01.this,1);

       registerNetworkBroadcastForNougat();
        NC.isOnline(getApplicationContext());

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner(getApplicationContext());


        context = getApplicationContext();



        intiview();



        interstitialAd = new InterstitialAd(this);

        /*Get Ad Unit ids*/
        interstitial0 = AdMob.get_admob_id(0, INTERSTITIAL_KEY);
        interstitial1 = AdMob.get_admob_id(1, INTERSTITIAL_KEY);
        interstitial2 = AdMob.get_admob_id(2, INTERSTITIAL_KEY);
        interstitial3 = AdMob.get_admob_id(3, INTERSTITIAL_KEY);
        interstitial4 = AdMob.get_admob_id(4, INTERSTITIAL_KEY);

        TextView title = (TextView) findViewById(R.id.textView_scn02);
        title.setText(singleton_images.getInstance().getApp_name());
        //(singleton_images.getInstance().getApp_name());

        interstitialAd.setAdUnitId(interstitial0);
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please wait ..." + "Ad Loading ...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);

        c = getApplicationContext();

        Intent i = getIntent();
        ads_variable = i.getBooleanExtra("ads", true);

        // timestamp=new java.sql.Timestamp(this);
        current_time = new Date().getTime();
        Log.d("current time :", String.valueOf(current_time));

        String DUMMY_IMAGE = "https://dummyimage.com/400x400/000/fff&text=Mehndi";
        String DUMMY_TEXT = "Mehndi Designs Set 1";

        //Glide.with(getApplicationContext()).load("19-bridal-mehndi-designs-by-amelia.jpg").into(img1);
        /*Load Images into buttons*/
        singleton_images singleton = singleton_images.getInstance();
        load_Buttons(txt1, img1, singleton.get_set_json(1));
        load_Buttons(txt2, img2, singleton.get_set_json(2));
        load_Buttons(txt3, img3, singleton.get_set_json(3));
        load_Buttons(txt4, img4, singleton.get_set_json(4));
        load_Buttons(txt5, img5, singleton.get_set_json(5));
        load_Buttons(txt6, img6, singleton.get_set_json(6));
        load_Buttons(txt7, img7, singleton.get_set_json(7));
        load_Buttons(txt8, img8, singleton.get_set_json(8));
        load_Buttons(txt9, img9, singleton.get_set_json(9));
        load_Buttons(txt10, img10, singleton.get_set_json(10));



        if(NC.network_status==Boolean.FALSE ) {
            //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();

            CD.show();
        }

        if (NC.network_status == Boolean.TRUE ) {


            layout_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //interstitialAd.setAdUnitId(interstitial0);

                    Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        CD.show();
                    }
                    if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                        pd.show();
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                            Bundle b = new Bundle();
                            b.putInt("flag", 1);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();


                        }


                        if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE && NC.json_status == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 1);
                                    i.putExtras(b);
                                    pd.dismiss();
                                    if (NC.isOnline(getApplicationContext()) == Boolean.TRUE && NC.json_status == Boolean.TRUE) {
                                        startActivity(i);
                                    }
                                    else {
                                        CD.show();
                                        pd.dismiss();
                                    }


                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);

                                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE || NC.json_status == Boolean.FALSE){
                                        CD.show();
                                        pd.dismiss();
                                    }
                                    Intent j = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 1);
                                    j.putExtras(b);

                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });

            layout_2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NETWORK", "Layout2 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        CD.show();

                    }
                    if (NC.network_status == Boolean.TRUE) {
                        pd.show();
                        //pd.setCancelable(true);
                        //interstitialAd.setAdUnitId(interstitial1);


                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(2);
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                            Bundle b = new Bundle();
                            b.putInt("flag", 2);
                            i.putExtras(b);
                            pd.dismiss();
                            startActivity(i);

                        }
                        if (ad1_status == Boolean.TRUE) {

                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

                                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 2);
                                    i.putExtras(b);
                                    pd.dismiss();
                                    startActivity(i);
                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });
            layout_3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NETWORK", "Layout3 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        CD.show();
                    }
                    if (NC.network_status == Boolean.TRUE) {
                        pd.show();
                        //pd.setCancelable(true);
                        //interstitialAd.setAdUnitId(interstitial2);

                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(3);
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);

                            Bundle b = new Bundle();
                            b.putInt("flag", 3);
                            i.putExtras(b);
                            pd.dismiss();
                            startActivity(i);

                        }
                        if (ad1_status == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

                                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 3);
                                    i.putExtras(b);
                                    startActivity(i);
                                    pd.dismiss();
                                }

                                @Override
                                public void onAdFailedToLoad(int i) {

                                    super.onAdFailedToLoad(i);
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });
            layout_4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NETWORK", "Layout4 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        CD.show();
                    }
                    if (NC.network_status == Boolean.TRUE) {
                        pd.show();
                        //pd.setCancelable(true);

                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(4);
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                            Bundle b = new Bundle();
                            b.putInt("flag", 4);
                            i.putExtras(b);
                            pd.dismiss();
                            startActivity(i);

                        }
                        if (ad1_status == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

                                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 4);
                                    i.putExtras(b);
                                    startActivity(i);
                                    pd.dismiss();
                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });
            layout_5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NETWORK", "Layout5 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        CD.show();
                    }
                    if (NC.network_status == Boolean.TRUE) {

                        pd.show();
                        //pd.setCancelable(true);

                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(5);
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                            Bundle b = new Bundle();
                            b.putInt("flag", 5);
                            i.putExtras(b);
                            pd.dismiss();
                            startActivity(i);

                        }
                        if (ad1_status == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

                                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 5);
                                    i.putExtras(b);
                                    startActivity(i);
                                    pd.dismiss();
                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });

            layout_6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NETWORK", "Layout6 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        CD.show();
                    }
                    if (NC.network_status == Boolean.TRUE) {
                        pd.show();
                        //pd.setCancelable(true);

                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(6);
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                            Bundle b = new Bundle();
                            b.putInt("flag", 6);
                            i.putExtras(b);
                            pd.dismiss();
                            startActivity(i);

                        }
                        if (ad1_status == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

                                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 6);
                                    i.putExtras(b);
                                    startActivity(i);
                                    pd.dismiss();
                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });
            layout_7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NETWORK", "Layout7 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        CD.show();
                    }
                    if (NC.network_status == Boolean.TRUE) {
                        pd.show();
                        //pd.setCancelable(true);

                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(7);
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                            Bundle b = new Bundle();
                            b.putInt("flag", 7);
                            i.putExtras(b);
                            pd.dismiss();
                            startActivity(i);

                        }
                        if (ad1_status == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

                                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 7);
                                    i.putExtras(b);
                                    startActivity(i);
                                    pd.dismiss();
                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });
            layout_8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NETWORK", "Layout8 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        CD.show();
                    }
                    if (NC.network_status) {
                        pd.show();
                        //pd.setCancelable(true);

                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(8);
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                            Bundle b = new Bundle();
                            b.putInt("flag", 8);
                            i.putExtras(b);
                            pd.dismiss();
                            startActivity(i);

                        }
                        if (ad1_status == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

                                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 8);
                                    i.putExtras(b);
                                    startActivity(i);
                                    pd.dismiss();
                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });
            layout_9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NETWORK", "Layout9 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        CD.show();
                    }
                    if (NC.network_status == Boolean.TRUE) {
                        pd.show();
                        //pd.setCancelable(true);

                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(9);
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                            Bundle b = new Bundle();
                            b.putInt("flag", 9);
                            i.putExtras(b);
                            pd.dismiss();
                            startActivity(i);


                        }
                        if (ad1_status == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

                                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 9);
                                    i.putExtras(b);
                                    startActivity(i);
                                    pd.dismiss();
                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });
            layout_10.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        //Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        CD.show();
                    }
                    if (NC.network_status == Boolean.TRUE) {
                        pd.show();
                        //pd.setCancelable(true);

                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(10);
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                            Bundle b = new Bundle();
                            b.putInt("flag", 10);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();

                        }
                        if (ad1_status == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

                                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                                    Bundle b = new Bundle();
                                    b.putInt("flag", 10);
                                    i.putExtras(b);
                                    startActivity(i);
                                    pd.dismiss();
                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });


        }
    }

    private void intiview() {



        img1=(ImageView) findViewById(R.id.btn1);
        img2=(ImageView) findViewById(R.id.btn2);
        img3=(ImageView) findViewById(R.id.btn3);
        img4=(ImageView) findViewById(R.id.btn4);
        img5=(ImageView) findViewById(R.id.btn5);
        img6=(ImageView) findViewById(R.id.btn6);
        img7=(ImageView) findViewById(R.id.btn7);
        img8=(ImageView) findViewById(R.id.btn8);
        img9=(ImageView) findViewById(R.id.btn9);
        img10=(ImageView) findViewById(R.id.btn10);

        txt1=(TextView)findViewById(R.id.txt1);
        txt2=(TextView)findViewById(R.id.txt2);
        txt3=(TextView)findViewById(R.id.txt3);
        txt4=(TextView)findViewById(R.id.txt4);
        txt5=(TextView)findViewById(R.id.txt5);
        txt6=(TextView)findViewById(R.id.txt6);
        txt7=(TextView)findViewById(R.id.txt7);
        txt8=(TextView)findViewById(R.id.txt8);
        txt9=(TextView)findViewById(R.id.txt9);
        txt10=(TextView)findViewById(R.id.txt10);

        layout_1=(LinearLayout)findViewById(R.id.btn_1);
        layout_2=(LinearLayout)findViewById(R.id.btn_2);
        layout_3=(LinearLayout)findViewById(R.id.btn_3);
        layout_4=(LinearLayout)findViewById(R.id.btn_4);
        layout_5=(LinearLayout)findViewById(R.id.btn_5);
        layout_6=(LinearLayout)findViewById(R.id.btn_6);
        layout_7=(LinearLayout)findViewById(R.id.btn_7);
        layout_8=(LinearLayout)findViewById(R.id.btn_8);
        layout_9=(LinearLayout)findViewById(R.id.btn_9);
        layout_10=(LinearLayout)findViewById(R.id.btn_10);




    }




    public void load_Buttons(TextView textView, ImageView imgvButton, JSONObject set_json) {

        try {
            Glide.with(getApplicationContext()).load(set_json.getString("set_image_url"))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgvButton);
            textView.setText(set_json.getString("title"));

        } catch (Exception e) {
            e.printStackTrace();
        }




    }


    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Intent i = new Intent(Activity_01.this,Activity_00.class);
        startActivity(i);
        return super.onKeyDown(keyCode, event);
    }
}
