package com.mindgame.image;

import android.content.Context;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.engine.cache.DiskLruCacheFactory;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.module.GlideModule;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;

import java.io.File;

/**
 * Created by kalyani on 9/7/2017.
 */

public class GlideSetup implements GlideModule {
    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        builder.setDiskCache(
                new InternalCacheDiskCacheFactory1(context, "GlideTestCache", 1000000000));

    }

    @Override
    public void registerComponents(Context context, Glide glide) {

    }

    public final class InternalCacheDiskCacheFactory1 extends DiskLruCacheFactory {

        public InternalCacheDiskCacheFactory1(Context context) {
            this(context, DiskCache.Factory.DEFAULT_DISK_CACHE_DIR, DiskCache.Factory.DEFAULT_DISK_CACHE_SIZE);
        }

        public InternalCacheDiskCacheFactory1(Context context, int diskCacheSize) {
            this(context, DiskCache.Factory.DEFAULT_DISK_CACHE_DIR, diskCacheSize);
        }

        public InternalCacheDiskCacheFactory1(final Context context, final String diskCacheName, int diskCacheSize) {
            super(new CacheDirectoryGetter() {
                @Override
                public File getCacheDirectory() {
                    File cacheDirectory = context.getFilesDir();
                    if (cacheDirectory == null) {
                        return null;
                    }
                    if (diskCacheName != null) {
                        return new File(cacheDirectory, diskCacheName);
                    }
                    Log.d("GLIDE", cacheDirectory.toString());
                    return cacheDirectory;
                }
            }, diskCacheSize);
        }
    }



}
