package com.mindgame.image;

/**
 * Created by kalyani on 8/7/2017.
 */

class SAFS {
    private static final SAFS ourInstance = new SAFS();

    static SAFS getInstance() {
        return ourInstance;
    }

    private SAFS() {
    }
}
