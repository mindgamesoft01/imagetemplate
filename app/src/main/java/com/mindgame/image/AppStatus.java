package com.mindgame.image;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


public class AppStatus {

    private static AppStatus instance = new AppStatus();
    static Context context;
    ConnectivityManager connMgr  ;
    NetworkInfo wifiInfo, mobileInfo;
    boolean connected_status;

    public static AppStatus getInstance(Context ctx) {
        context = ctx.getApplicationContext();
        return instance;
    }

    public boolean networkState(Context ctx){

        ConnectivityManager cm =
                (ConnectivityManager)ctx.getSystemService(ctx.CONNECTIVITY_SERVICE);

        if(cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED || cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
        {
            return connected_status=Boolean.TRUE;
        }


        else if(cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED || cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED){

        }
        return connected_status=Boolean.FALSE;

    }




    public boolean isOnline(Context ctx) {
        try {


            ConnectivityManager cm =
                    (ConnectivityManager)ctx.getSystemService(ctx.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
            return isConnected;

        } catch (Exception e) {
            System.out.println("CheckConnectivity Exception: " + e.getMessage());
            Log.v("connectivity", e.toString());
            return Boolean.FALSE;
        }

    }
}