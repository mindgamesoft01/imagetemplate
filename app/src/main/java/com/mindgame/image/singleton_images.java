package com.mindgame.image;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by kalyani on 5/4/2017.
 */

public class singleton_images {
    public long lastIntestitial_loadded=0;
    public String admob_mode, image_mode;

    public String getApp_name() {
        return app_name;
    }

    public int getInterstitial_timer() {
        return interstitial_timer;
    }

    public void setInterstitial_timer(int interstitial_timer) {
        this.interstitial_timer = interstitial_timer;
    }

    public int interstitial_timer=0;

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String app_name = "";
    public String app_mode = "";
    public String BANNER_TEST_ID = "ca-app-pub-3940256099942544/6300978111";
    public String INTERSTITIAL_TEST_ID = "ca-app-pub-3940256099942544/1033173712";

    public long getLastIntestitial_loadded() {
        return lastIntestitial_loadded;
    }

    public void setLastIntestitial_loadded(long lastIntestitial_loadded) {
        this.lastIntestitial_loadded = lastIntestitial_loadded;
    }






    public Log getIntertitial_lastloaded() {
        return intertitial_lastloaded;
    }

    public void setIntertitial_lastloaded(Log intertitial_lastloaded) {
        this.intertitial_lastloaded = intertitial_lastloaded;
    }

    Log intertitial_lastloaded;
    private static final singleton_images ourInstance = new singleton_images();

    public JSONArray getImages_json() {
        return images_json;
    }

    public void setImages_json(JSONArray images_json) {
        Log.d("images", images_json.toString());
        this.images_json = images_json;
    }

    public JSONArray images_json;

    public JSONObject getAdmob_details() {
        return admob_details;
    }

    public void setAdmob_details(JSONObject admob_details) {

        this.admob_details = admob_details;
    }

    public JSONObject admob_details;

    public JSONArray getAd_details() {
        return ad_token;
    }

    public void setAd_details(JSONArray ad_token) {
        this.ad_token = ad_token;
    }

    private JSONArray ad_token;

    public void setAd_details(JSONObject ad_details) {
        this.ad_details = ad_details;
    }

    private JSONObject ad_details;

    static singleton_images getInstance() {
        return ourInstance;
    }

    private singleton_images() {
   }

    /*Return a JSON Object of of the given index from child images_json*
     *
     * @return
     *-------------------------------------------------*/
   public JSONObject get_set_json(int index){
       index = index -1;
        JSONObject set_json = new JSONObject();
       try {

            set_json = images_json.getJSONObject(index);

       }
       catch (Exception e){
           e.printStackTrace();
       }

       return set_json;
   }

    /*Returns an array list with child images for a set*/
    public ArrayList<String> get_set_images(int index){
        index = index -1;// In the images index is  1-10
        ArrayList <String> set_images = new ArrayList<String>();
        try {
                JSONArray tmp =  images_json.getJSONObject(index).getJSONArray("child_images");
                String bucket = images_json.getJSONObject(index).getString("bucket"); /*Bucket is same for all*/
                String folder = images_json.getJSONObject(index).getString("folder"); /*Folder is same for all*/
            for(int i =0; i<tmp.length(); i++ ){
                String set_img_url =bucket + folder+"/" + tmp.getString(i);
                set_images.add(set_img_url);
            }



        }
        catch (Exception e){
            e.printStackTrace();
        }

        return set_images;
    }

    /*Returns an array list with child THUMB images for a set*/
    public ArrayList<String> get_set_images_thumbs(int index){
        index = index -1;// In the images index is  1-10
        ArrayList <String> set_images_thumbs = new ArrayList<String>();
        try {
                JSONArray tmp =  images_json.getJSONObject(index).getJSONArray("child_images");
                String bucket = images_json.getJSONObject(index).getString("bucket"); /*Bucket is same for all*/
                String folder = images_json.getJSONObject(index).getString("folder"); /*Folder is same for all*/
            for(int i =0; i<tmp.length(); i++ ){

                String set_img_url_thumb =bucket + folder+"/" + "thumbs" + "/" + tmp.getString(i);
                set_images_thumbs.add(set_img_url_thumb);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }

        return set_images_thumbs;
    }


    public Boolean get_ad_status(int index) {
        Boolean ad_status = Boolean.TRUE;

        try {
            String set_string = "set_" + String.valueOf(index);
            JSONObject tmp =  ad_token.getJSONObject(index);
            Log.d("ad_token", tmp.toString());

            Log.d("admob_mode:", admob_mode);

            /*Check the time stamp difference*/
            if (lastIntestitial_loadded != 0) {
                //Diff in seconds
                long difference = (new Date().getTime() - lastIntestitial_loadded) / 1000;
                /* Show ads when
                 *  1. Time Diff >1 min
                 *  2. ad_token = True
                  * 3. admob_mode  <> hold
                  * */

                if ((difference >= interstitial_timer) && tmp.getBoolean(set_string) && !admob_mode.equals("hold")) {
                    ad_status = Boolean.TRUE;
                } else {
                    ad_status = Boolean.FALSE;
                }
            } else {
                ad_status = tmp.getBoolean(set_string) && !admob_mode.equals("hold");
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
        return ad_status;
    }

 }
